Program Pascalzim ;
uses graph ;
var driver, modo: integer ;
		x, y, raio, deslocamento, tamanhoTile, tamanhoTileDesenhar : integer;
		posicao : integer;
		tecla : char;
		
procedure desenharTexto(x, y, alturaLinha : integer; texto : string);
var contCarac, contLinhas : integer; 
		strTemporaria : string;
begin
	// Resetar as vari�veis por seguran�a
	contCarac:= 1; contLinhas:= 0; strTemporaria:= '';
	// Se o par�metro da altura for 0, usar a altura natural do texto
	if alturaLinha = 0 then alturaLinha:= textHeight(texto);
	
	// Loop que vai repetir por todos os caracteres do texto passado
	while contCarac < length(texto) do
	begin
		// Se detectarmos um caractere de barra, teremos que verificar o pr�ximo caractere pra saber qual a��o tomar
		if (texto[contCarac] = '\') then
		begin
			// Se o pr�ximo caractere for uma barra, seguindo de um n ('\\n'), significa que � um \n escapado, portanto agir normalmente
			if ((texto[contCarac+1] = '\') and (texto[contCarac+2] = 'n')) then
			begin
				// Pular os tr�s caracteres do \\n
				contCarac:= contCarac+3;
				// Adicionar o \n na string, sem a primeira barra
				strTemporaria:= strTemporaria + '\n';
			end
			// Se o pr�ximo caractere for um n ('\n'), ent�o � um linebreak
			else if (texto[contCarac+1] = 'n') then
			begin
				// Escrever a string que obtemos at� aqui na tela
				outTextXY(x, y + (alturaLinha * contLinhas), strTemporaria);
				// Resetar a string tempor�ria, j� que come�aremos uma nova linha
				strTemporaria:= '';
				contLinhas:= contLinhas + 1;			
				contCarac:= contCarac + 2; // +2 pra pular o \n e ir pro pr�ximo caractere depois deles
			end
			// Se for s� uma barra no meio do texto, agir normalmente
			else
			begin
				strTemporaria:= strTemporaria + texto[contCarac];
				contCarac:= contCarac + 1;	
			end;
		end
		// Se n�o for uma \, vai jogando os caracteres na strTempor�ria at� chegar no linebreak.
		else
		begin
			strTemporaria:= strTemporaria + texto[contCarac];
			contCarac:= contCarac + 1;
		end;
	end;		
end;

Begin
 // Inicializa o modo grafico
 driver := Detect;
 initgraph(driver, modo, '');
 
 // Verifica se a inicializacao foi feita com sucesso
 if(graphResult <> grOk) then
 begin
   writeln('Erro ao inicializar o modo grafico:', GraphErrorMsg(graphResult));
   exit;
 end;
 
 // -------------------------------------------------------------------------------------
 // IN�CIO DA SANDBOX
 // -------------------------------------------------------------------------------------
	
	setTextStyle(SmallFont, HorizDir, 8); 
 	setTextJustify(RightText, LeftText);
 	setColor(LIGHTGREEN);
	desenharTexto(int(640/2), int(480/2), 0, 'The quick \n brown fox \n jumps over \n the lazy dog \n .');
	
	
	repeat
	tecla:= readkey();
	case upcase(tecla) of
	#0 : case (readkey) of
		#72 : begin
			setFillStyle(SolidFill, red);
			bar(x, y, x + 20, y + 20);	
		end;
		
		#80 : begin
			setFillStyle(SolidFill, green);
			bar(x, y, x + 20, y + 20);
		end;
				
		#77 : begin
			setFillStyle(SolidFill, yellow);
			bar(x, y, x + 20, y + 20);
		end;
				
		#75 : begin
			setFillStyle(SolidFill, blue);
			bar(x, y, x + 20, y + 20);
		end;
	end;
	#13 : begin
			setFillStyle(SolidFill, lightgray);
			bar(x, y, x + 20, y + 20);	
		end;
end;
	until false;	
	{	// ItSlashFill aparece no arquivo de ajuda mas o compilador diz que ele n�o foi declarado
	setFillStyle(ItSlashFill, WHITE);
	bar(x, y, x + 20, y + 20);
	x:= x + 30;}
	 

 // -------------------------------------------------------------------------------------
 // FIM DA SANDBOX
 // -------------------------------------------------------------------------------------
 // Fecha o modo grafico
 readkey;
 closegraph;
End.

